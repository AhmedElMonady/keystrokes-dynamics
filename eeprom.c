#include "tm4c123gh6pm.h"
#include "eeprom.h"
//Initialise EEPROM
void initEEPROM()
{
  SYSCTL_RCGCEEPROM_R=1;
}
//write 4 byte word to EEPROM
//input (block) block to be written to,(offset) word to be written to,(data) 4 byte data to save
void EEPROM_writeWord(unsigned char block,unsigned char offset,unsigned int data)
{
  while(EEPROM_EEDONE_R!=0);
  EEPROM_EEBLOCK_R=block;
  EEPROM_EEOFFSET_R=offset;
  EEPROM_EERDWR_R=data;
}
//read 4 byte word from EEPROM
//input (block) block to be read from,(offset) word to be read from
//returns 4 byte data from chosen word
unsigned int EEPROM_readWord(unsigned char block,unsigned char offset)
{
  while(EEPROM_EEDONE_R!=0);
  EEPROM_EEBLOCK_R=block;
  EEPROM_EEOFFSET_R=offset;
  return EEPROM_EERDWR_R;
}
//writes 2 bytes to upper half-word
//input (block) block to be written to,(offset) word to be written to,(data) 2 byte data to save
void EEPROM_writeHHW(unsigned char block,unsigned char offset,unsigned short data)
{
  unsigned int temp=EEPROM_readWord(block,offset);
  temp&=0xFFFF;
  temp|=(data<<16);
  EEPROM_writeWord(block,offset,temp);
}
//writes 2 bytes to lower half-word
//input (block) block to be written to,(offset) word to be written to,(data) 2 byte data to save
void EEPROM_writeLHW(unsigned char block,unsigned char offset,unsigned short data)
{
  unsigned int temp=EEPROM_readWord(block,offset);
  temp&=0xFFFF0000;
  temp|=data;
  EEPROM_writeWord(block,offset,temp);
}
//reads 2 bytes from upper half-word
//input (block) block to be read from,(offset) word to be read from
//returns 2 byte halfword
unsigned short EEPROM_readHHW(unsigned char block,unsigned char offset)
{
  unsigned int temp=EEPROM_readWord(block,offset);
  return (temp>>16);
}
//reads 2 bytes from lower half-word
//input (block) block to be read from,(offset) word to be read from
//returns 2 byte halfword
unsigned short EEPROM_readLHW(unsigned char block,unsigned char offset)
{
  unsigned int temp=EEPROM_readWord(block,offset);
  return temp&(0xFFFF);
}
//returns number of saved users in EEPROM
unsigned char EEPROM_getNumOfUsers()
{
  return EEPROM_readWord(BLOCK1,0);
}
//Adds new user to EEPROM
//input: (data) 2D array of short of size 30*10 consists of data about user's travel time between keys for each first 9 values
//and the user's number each 10th value
//(name) user name consisting of 4 bytes(4 ASCII characters)
void EEPROM_addProfile(unsigned short data[30][10],unsigned int name)
{
  unsigned char users;
  unsigned char blockCount=0;
  users=EEPROM_getNumOfUsers();
  users++;
  unsigned char currentBlock=(10*users)-8;
  EEPROM_writeWord(BLOCK1,0,users);
  EEPROM_writeWord(BLOCK1,users,name);
  for(unsigned char i=0;i<30;i++)
  {
    for(unsigned char j=0;j<10;j++)
    {
      if((j%2)==0)
      {
        EEPROM_writeLHW(currentBlock,blockCount,data[i][j]);
      }
      else
      {
        EEPROM_writeHHW(currentBlock,blockCount,data[i][j]);
        blockCount++;
      }
      if(blockCount>15)
      {
        currentBlock+=1;
        blockCount=0;
      }
    }
  }  
}
//returns all saved data about users on EEPROM
//input: (dataset) pointer to type short array that should accomodate (300*number of users)+1 shorts values
//data returned is ready to be used as input for ID3 decision tree algorithm
void EEPROM_getProfiles(unsigned short** dataset)
{
  unsigned char users=EEPROM_getNumOfUsers();
  unsigned char blockCount=0;
  unsigned char currentBlock;
  for(unsigned char u=1;u<=users;u++)
  {
    currentBlock=(10*u)-8;
    blockCount=0;
    for(unsigned char i=0;i<10;i++)
    {
		for(unsigned char j=0;j<10;j++)
		{
		  if((j%2)==0)
		  {
			dataset[i+(10*(u-1))][j]=EEPROM_readLHW(currentBlock,blockCount);
		  }
		  else
		  {
			dataset[i+(10*(u-1))][j]=EEPROM_readHHW(currentBlock,blockCount);
			blockCount++;
		  }
		  if(blockCount>15)
		  {
			currentBlock+=1;
			blockCount=0;
		  }
		}
    }
  }
}
//get 4 byte username for a given user stored in EEPROM
//input: (id)-user number
unsigned int EEPROM_getUsername(unsigned char id)
{
  return EEPROM_readWord(BLOCK1,id);
}
void eraseProfiles()
{
  EEPROM_writeWord(BLOCK1,0,0);
}
unsigned int EEPROM_encodeName(unsigned char letter[])
{
	unsigned int name = 0;
	for (unsigned char i = 0; i<4; i++)
	{
		name |= letter[i];
		if (i != 3)
		{
			name = (name << 8);
		}
	}
	return name;
}
void EEPROM_decodeName(unsigned int name, unsigned char letter[])
{
	letter[0] = (name >> 24);
	letter[1] = (name >> 16);
	letter[2] = (name >> 8);
	letter[3] = name;
}


