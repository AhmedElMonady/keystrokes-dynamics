#include "tm4c123gh6pm.h"
#include "eeprom.h"
#include "DTC.h"
void UART0_CTRL(char en);
void write_string(char * str);

//a gloabal variable to count number of times of overflow 
volatile unsigned int ofcount;
//flag to stop the main
unsigned char stop = 0;
unsigned char identifying = 0;

unsigned char no_of_users;
unsigned short user_data[30][10];

void filter()
{
  unsigned int sum;
  unsigned char count;
  unsigned short avg;
  
  for(unsigned char i = 0; i < 9; i++)
  {
    count = 0;
    sum = 0;
    avg = 0;
    for(unsigned char j = 0; j < 30; j++)
    {
      if(user_data[j][i] < 6000)
      {
        count++;
        sum += user_data[j][i];
      }
    }
    avg = sum / count;
    for(unsigned char j = 0; j < 30; j++)
    {
      if(user_data[j][i] >= 6000)
      {
        user_data[j][i] = avg;
      }
    }
  }
}

void delayMs(int ms)
{
  int i, j;
  for(i = 0; i<500; i++)
  {
    for(j=0; j<3180; j++);
  }
}

// a function to handle received character by uart 
void UART0Handler(void)
{
  static unsigned int previous, current;
  static unsigned char column_counter, row_counter;
  unsigned short time;
  volatile int readback;
  unsigned char c ;
  
  // to add if enter is pressed + counter of no of entries (disable uart interrupt after 30 time has finished)
    
  //check RXMIS flag in case of received char
  if ( (UART0_MIS_R & (0x10)) )
  {
    no_of_users = EEPROM_getNumOfUsers();
    // store data received in char c 
    c = UART0_DR_R;
    
    // reset overflow counter 
    current = TIMER1_TAR_R;
    
    // case first char entered 
    if ( previous != 0 )
    {
       // count number of ticks elapsed between current and previous 
      unsigned int ticks = (current - previous) + (ofcount * 0xFFFF);
      
      // calculate time (ms)
      time = ticks / 16000;
      user_data[row_counter][column_counter - 1] = time;
    }
      
    //echo received data 
    while((UART0_FR_R & 0x20) != 0);
    UART0_DR_R = c;
    
    column_counter++;
    if(column_counter > 9)
    {
      column_counter = 0;
      row_counter++;
      previous = 0;
      current = 0;
      user_data[row_counter - 1][9] = no_of_users + 1;
      write_string("\r\n");
    }
    if(identifying == 0){
      if(row_counter > 29)
      {
        column_counter = 0;
        row_counter = 0;
        previous = 0;
        current = 0;
        stop = 0;
        UART0_CTRL('0');
      }
    }
    else
    {
      if(row_counter > 0)
      {
        column_counter = 0;
        row_counter = 0;
        previous = 0;
        current = 0;
        stop = 0;
        UART0_CTRL('0');
      }
    }
   
    previous = current;
    ofcount = 0;
    
    //clear interrupt flag (RXIS)
    UART0_ICR_R &= ~(1<<4);
    
    // read back 
    readback = UART0_ICR_R;             // a readback forces to clear interrupt 
  }
}

// a fuction to handle timeout of timer1A
void Timer_1A_Handler(void)
{
  volatile int readback;
  if((TIMER1_MIS_R & (1<<0)))                   //check if timer has timed out
  {
     //increment overflow counter 
     ofcount++;
     
     //clear interrupt flag 
     TIMER1_ICR_R = 0x1;
     
     //read back forces clearing interrupt flag 
     readback = TIMER1_ICR_R;
  }
}

// a function to configure UART (PORT A MODULE 0)
void UART_config(void)
{
  //enable clock for UART0
  SYSCTL_RCGCUART_R |= 1;
  
  //run clock gating system for port A 
  SYSCTL_RCGCGPIO_R |= 1;
  
  //diasable uart for configuration 
  UART0_CTL_R &= ~(1<<0);
  
  //configure baudrate 
  UART0_IBRD_R = 104;                   //integer divisor 
  UART0_FBRD_R = 11;                    //float divisor 
  
  //select clock source 
  UART0_CC_R = 0;
  
  // no parity, one stop bit, word length 
  UART0_LCRH_R = 0x60;
  
  //make an interrupt when receiving 
  UART0_IM_R |= 0x10;
  
  //enable uart 
  UART0_CTL_R |=(1<<0);
  
  //digital enable PA0 and PA1   
  GPIO_PORTA_DEN_R |= 0x3;
  
  //set alternate function pins for PA0 and PA1
  GPIO_PORTA_AFSEL_R |= 0x3;
  
  //select UART as alternate function for pins 
  GPIO_PORTA_PCTL_R |= 0x11;
  
  //for interrupts 
  NVIC_PRI1_R |= 0x3000;                 //set priority  

  //enable global interrupts 
  __asm("CPSIE  I");
}

// a function to control interrupts for uart (1 = enable , 0 = disable )
void UART0_CTRL(char en)
{
  if(en == '1')
  {
    NVIC_EN0_R |= 0x20;                     //enable irq register for uart0
  }
  else
  {
    NVIC_DIS0_R |= 0x20;                   //disable irq register for uart0 
  }  
}

// a function to configure timer block 1 in module A 
void Timer1_config(void)
{
  //enable clock for module 1 
  SYSCTL_RCGCTIMER_R |= 0x2;
  
  // disable timer for configuration 
  TIMER1_CTL_R = 0;                       //TAEN = 0
  
  // select the 16 bit mode 
  TIMER1_CFG_R = 0x4;
  
  // select down counting and periodic mode 
  TIMER1_TAMR_R = 0x2;
  
  // set reload value 
  TIMER1_TAILR_R = 0xFFFF; 
  
  //clear time out (interrupt mask)
  TIMER1_ICR_R = 0x1;
  
  //enable timer 1 timeout interrupt 
  TIMER1_IMR_R |= 0x1;
  
  //enable timer
  TIMER1_CTL_R |= 0x1;
  
  //enable irq21 
  NVIC_EN0_R |= 0x200000;
}


// a function to write a string 
void write_string(char * str)
{
  unsigned char i = 0;
  while ( str[i] != 0)
  {
      while((UART0_FR_R & 0x20) != 0);
      UART0_DR_R = str[i];
      i++;
  }
}

unsigned char UART_RX(void)
{
  unsigned char c;
  while((UART0_FR_R & 0x10) != 0);
  c = UART0_DR_R;
  return c;
}

void UART_TX(unsigned char c)
{
  while((UART0_FR_R & 0x20) != 0);
  UART0_DR_R = c;
}

int main()
{
  //enable global interrupts (CPSID I for disable)
  __asm("CPSIE  I");
  
  //initialize EEPROM
  initEEPROM();
  //usersG=EEPROM_getNumOfUsers();
  //initiallize uart 
  UART_config();
  
  //initiallize timer 
  Timer1_config();
  
  no_of_users = EEPROM_getNumOfUsers();
  unsigned char mode;
  
  while(1)
  {
    write_string("\r\nSelect Mode:\r\n(1): Add new user\r\n(2): Identify user\r\n(3): Erase all users\r\n");
    mode = UART_RX();
    UART_TX(mode);
    write_string("\r\n");
    while((mode != '1') && (mode != '2')&& (mode != '3'))
    {
      write_string("Invalid choice\r\nSelect Mode:\r\n(1): Add new user\r\n(2): Identify user\r\n(3): Erase all users\r\n");
      mode = UART_RX();
      UART_TX(mode);
    }
    
    if(mode == '1')
    {
      unsigned char check = EEPROM_getNumOfUsers();
      if(check < 3)
      {
        write_string("\r\nType the following word 30 times. (.tie4Ronal)\r\n");
        stop = 1;
        NVIC_EN0_R |= 0x200000;
        UART0_CTRL('1');
        while(stop == 1);
        filter();
        write_string("\r\n Enter user name (four characters):\r\n");
        unsigned char name[4];
        for(unsigned char i = 0; i < 4; i++)
        {
          name[i] = UART_RX();
          UART_TX(name[i]);
        }
        write_string("\r\n");
        unsigned int encoded_name = EEPROM_encodeName(name);
        EEPROM_addProfile(user_data, encoded_name);
      }
      else
      {
        write_string("\r\nMaximum number of users reached\r\n");
      }
    }
    else if(mode =='2')
    {
      no_of_users = EEPROM_getNumOfUsers();
      unsigned char size = no_of_users * 10;
      unsigned short** records = (unsigned short**)malloc(sizeof(unsigned short*) * size);
      for(unsigned char i = 0; i < size; i++)
      {
        records[i] = (unsigned short*)malloc(sizeof(unsigned short) * 10);
      }
      EEPROM_getProfiles(records);
      struct Node root = build_tree(records, size, no_of_users);
      identifying = 1;
      stop = 1;
      write_string("\r\nType (.tie4Ronal):\r\n");
      NVIC_EN0_R |= 0x200000;
      UART0_CTRL('1');
      while(stop == 1);
      identifying = 0;
      struct Prediction pred = predict(user_data[0], root, no_of_users);
      unsigned int encoded_name = EEPROM_getUsername(pred.label);
      unsigned char name[4];
      EEPROM_decodeName(encoded_name, name);
      write_string("\r\nUser: ");
      for(unsigned char i = 0; i < 4; i++)
      {
        UART_TX(name[i]);
      }
      delayMs(200);
      NVIC_APINT_R=((0x05FA)<<16)|(1<<2);
      for(unsigned char i = 0; i < size; i++)
      {
        free(records[i]);
      }
      free(records);
    }
    else
    {
      eraseProfiles();
      write_string("All users erased\r\n\r\n");
    }
    
  }

  return 0;
}
