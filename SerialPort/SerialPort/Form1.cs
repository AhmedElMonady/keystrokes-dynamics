﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace SerialPort
{
    public partial class Form1 : Form
    {
        static int counter = 0;
        static bool flag = false;
        public Form1()
        {
            InitializeComponent();
            cmdClose.Enabled = false;
            foreach (String s in System.IO.Ports.SerialPort.GetPortNames()) 
            {
                txtPort.Items.Add(s);
            }
            //txtDatatoSend.KeyDown += TxtDatatoSend_KeyDown;
            txtDatatoSend.KeyUp += TxtDatatoSend_KeyUp;
            //txtDatatoSend.KeyPress += TxtDatatoSend_KeyPress;
        }

        private void TxtDatatoSend_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode != Keys.CapsLock)
                button1_Click(sender, e);
        }

        private void TxtDatatoSend_KeyUp(object sender, KeyEventArgs e)
        {
            var key = e.KeyCode;
            if (key == Keys.D1 || key == Keys.D2 || key == Keys.D3)
            {
                flag = true;
                button1_Click(sender, e);
            }

            else if (Char.IsLetterOrDigit((char)e.KeyCode) || e.KeyCode == Keys.OemPeriod)
            {
                flag = false;
                button1_Click(sender, e);
            }
        }

        private void TxtDatatoSend_KeyPress(object sender, KeyPressEventArgs e)
        {
            button1_Click(sender, e);
        }

        public System.IO.Ports.SerialPort sport;

        public void serialport_connect(String port, int baudrate , Parity parity, int databits, StopBits stopbits) 
        {
            DateTime dt = DateTime.Now;
            String dtn = dt.ToShortTimeString();

            sport = new System.IO.Ports.SerialPort(
            port, baudrate, parity, databits, stopbits);
            try
            {
                sport.Open();
                cmdClose.Enabled = true;
                cmdConnect.Enabled = false;
                txtReceive.AppendText("[" + dtn + "] " + "Connected\r\n");
                sport.DataReceived += new SerialDataReceivedEventHandler(sport_DataReceived);
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString(), "Error"); }
        }

        private void sport_DataReceived(object sender, SerialDataReceivedEventArgs e) 
        {
            DateTime dt = DateTime.Now;
            String dtn = dt.ToShortTimeString();

            txtReceive.AppendText(/*"["+dtn+"] "+"Received: "+*/ sport.ReadExisting());
            if (!flag)
            {
                if (counter == 10)
                {
                    txtReceive.AppendText("\r\n");
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }
        }

        private void cmdConnect_Click(object sender, EventArgs e)
        {
            String port = txtPort.Text;
            int baudrate = Convert.ToInt32(cmbbaudrate.Text);
            Parity parity = (Parity)Enum.Parse(typeof(Parity), cmbparity.Text);
            int databits = Convert.ToInt32(cmbdatabits.Text);
            StopBits stopbits = (StopBits)Enum.Parse(typeof(StopBits), cmbstopbits.Text);
            
            serialport_connect(port, baudrate, parity, databits, stopbits);
            //serialport_connect(port, 9600, Parity.None, 8, StopBits.One);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            String dtn = dt.ToShortTimeString();
            int datalen = txtDatatoSend.Text.Length;
           if (datalen > 0)
            {
                String data = txtDatatoSend.Text.ToCharArray()[datalen - 1] + "";
                sport.Write(data);
                //txtReceive.AppendText(/*"[" + dtn + "] " + "Sent: " + */data + "\r\n");
                if (datalen == 10) txtDatatoSend.Text = String.Empty;
            }
            
        }

        private void cmdClose_Click_1(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            String dtn = dt.ToShortTimeString();

            if (sport.IsOpen) 
            {
                sport.Close();
                cmdClose.Enabled = false;
                cmdConnect.Enabled = true;
                txtReceive.AppendText("[" + dtn + "] " + "Disconnected\n");
            }
        }

        private void txtPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (String s in System.IO.Ports.SerialPort.GetPortNames())
            {
                txtPort.Items.Add(s);
            }
        }
    }
}
