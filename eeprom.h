#ifndef EEPROM_H_
#define EEPROM_H_

#define BLOCK0 0
#define BLOCK1 1
#define BLOCK2 2
#define BLOCK3 3
#define BLOCK4 4
#define BLOCK5 5
#define BLOCK6 6
#define BLOCK7 7
#define BLOCK8 8
#define BLOCK9 9
#define BLOCK10 10
#define BLOCK11 11
#define BLOCK12 12
#define BLOCK13 13
#define BLOCK14 14
#define BLOCK15 15
#define BLOCK16 16
#define BLOCK17 17
#define BLOCK18 18
#define BLOCK19 19
#define BLOCK20 20
#define BLOCK21 21
#define BLOCK22 22
#define BLOCK23 23
#define BLOCK24 24
#define BLOCK25 25
#define BLOCK26 26
#define BLOCK27 27
#define BLOCK28 28
#define BLOCK29 29
#define BLOCK30 30
#define BLOCK31 31

void initEEPROM();
void  EEPROM_writeWord(unsigned char block,unsigned char offset,unsigned int data);
unsigned int EEPROM_readWord(unsigned char block,unsigned char offset);
void EEPROM_writeHHW(unsigned char block,unsigned char offset,unsigned short data);
void EEPROM_writeLHW(unsigned char block,unsigned char offset,unsigned short data);
unsigned short EEPROM_readHHW(unsigned char block,unsigned char offset);
unsigned short EEPROM_readLHW(unsigned char block,unsigned char offset);
unsigned char EEPROM_getNumOfUsers();
void EEPROM_addProfile(unsigned short data[30][10],unsigned int name);
void EEPROM_getProfiles(unsigned short** dataset);
unsigned int EEPROM_getUsername(unsigned char id);
void eraseProfiles();
unsigned int EEPROM_encodeName(unsigned char letter[]);
void EEPROM_decodeName(unsigned int name, unsigned char letter[]);
#endif